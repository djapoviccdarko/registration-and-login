<?php
require_once 'DAO.php';
class userController
{
    public function loginButton()
    {
        $username = isset($_POST["username"]) ? test_input($_POST["username"]) : "";
        $password = isset($_POST["password"]) ? test_input($_POST["password"]) : "";

        if ($username == "" || $password == "") {
            $msg = "<p style='color: white;'>Fill in all fields!!</p>";
            include_once 'index.php';
        } else {

            $dao = new DAO;
            $user = $dao->selectUserByUsernameAndPassword($username, $password);
            if ($user) {
                $_SESSION['user'] = $user;
                $_SESSION['last-active'] = time();
                if (isset($_POST['remember'])) {
                    $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);

                    setcookie("userJSON", json_encode($userCookie), time() + 20, "/");
                }
                include_once $user['type'] . '.php';
            } else {
                $msg = "<p style='color: white;'>Wrong login parameters!!!</p>";
                include_once 'index.php';
            }
        }
    }
    public function registerButton()
    {
        $first_name = isset($_POST["first_name"]) ? test_input($_POST["first_name"]) : "";
        $last_name = isset($_POST["last_name"]) ? test_input($_POST["last_name"]) : "";
        $email = isset($_POST["email"]) ? test_input($_POST["email"]) : "";
        $username = isset($_POST["username"]) ? test_input($_POST["username"]) : "";
        $password = isset($_POST["password"]) ? test_input($_POST["password"]) : "";
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $repeat_password = isset($_POST["repeat_password"]) ? test_input($_POST["repeat_password"]) : "";
        $robot = isset($_POST["robot"]) ? test_input($_POST["robot"]) : "";

        if ($first_name == '' || $last_name == '' || $email == '' || $username == '' || $password == '' || $repeat_password == '') {
            $msg = '<br><span style="color: red;">Please fill in all fields!!';
            include_once 'register.php';
        } elseif ($robot == false) {
            $msg = '<br><span style="color: red;">Confirm you are not a robot!';
            include_once 'register.php';
        } elseif (!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            $msg = '<br><span style="color: red;">Password is not valid';
            include_once 'register.php';
        } elseif ($password != $repeat_password) {
            $msg = '<br><span style="color: red;">Passwords not matching';
            include_once 'register.php';
        } else {
            $dao = new DAO;
            if (!$dao->selectUserByUsername($username)) {
                if ($dao->insertUserWithAtributes($first_name, $last_name, $email, $username, $password) == false) {
                    $msg = '<br><span style="color: red;">Error!';
                    include_once 'register.php';
                } else {
                    include_once 'index.php';
                }
            } else {
                $msg = "<br><span style='color: red;'>Username '$username' is already in use!";
                include_once 'register.php';
            }
        }
    }
}
