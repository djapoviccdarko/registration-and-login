<?php 
$msg = isset($msg)?$msg:'';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/index-style.css">
    <title>Document</title>
</head>

<body>
    <div class="login-box">
        <h2>Login</h2>
        <form action="autController.php" method="POST">
            <div class="user-box">
                <input type="text" name="username" >
                <label >Username</label>
            </div>
            <div class="user-box">
                <input type="password" name="password">
                <label>Password</label>
            </div>
            <label style="color: white;"> Remember me: </label>
            <input type="checkbox" name="remember" value="yes"><br><br>
            <input type="submit" value="LOGIN" name="action" class="submit"><br>
            <a class="register" style="color:white" href="register.php"> Do not have an account?Register!</a><br>
            <?=$msg ?>

        </form>
    </div>
</body>

</html>