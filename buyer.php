<?php
session_start();
$msg = isset($msg) ? ($msg) : "";
require_once 'DAO.php';
$dao = new DAO();
$products = $dao->selectAllProducts();
if (isset($_POST["add_to_cart"])) {
    if (isset($_SESSION["shopping_cart2"])) {
        $item_array_id = array_column($_SESSION["shopping_cart2"], "item_id"); //vraca vrednosti iz kolone item_id
        if (!in_array($_GET["id"], $item_array_id)) {
            $count = count($_SESSION["shopping_cart2"]);
            $item_array = array(
                'item_id'            =>    $_GET["id"],
                'item_name'            =>    $_POST["hidden_name"],
                'item_price'        =>    $_POST["hidden_price"],
                'item_quantity'        =>    $_POST["quantity"]
            );
            $_SESSION["shopping_cart2"][$count] = $item_array;
            //nakon pritiskanja add to cart proizvodi se dodaju u item_array
            //ako proizvod sa odredjenim id-jem nije dodat u korpu, dodaj ga a ako jeste onda else
        } else {
            $msg = "Item is already added!";
        }
    } else {
        $item_array = array(
            'item_id'            =>    $_GET["id"],
            'item_name'            =>    $_POST["hidden_name"],
            'item_price'        =>    $_POST["hidden_price"],
            'item_quantity'        =>    $_POST["quantity"]
        );
        $_SESSION["shopping_cart2"][0] = $item_array;
    }
}
if (isset($_GET["action"])) {
    if ($_GET["action"] == "delete") {
        foreach ($_SESSION["shopping_cart2"] as $keys => $values) {
            if ($values["item_id"] == $_GET["id"]) {
                unset($_SESSION["shopping_cart2"][$keys]);
                include_once 'buyer.php';
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <h1>PRODUCTS</h1>
        <?php
        if (count($products) > 0) {
            foreach ($products as $pom) {
        ?>

                <form method="POST" action="buyer.php?action=add&id=<?php echo $pom["id"]; ?>">
                    <h4><?php echo $pom['name']; ?></h4>
                    <h4><?php echo $pom['price']; ?></h4>
                    Quantity: <br>
                    <input type="text" name="quantity" value="1">
                    <input type="hidden" name="hidden_name" value="<?php echo $pom["name"]; ?>" id="">
                    <input type="hidden" name="hidden_price" value="<?php echo $pom["price"]; ?>" id="">
                    <input type="submit" name="add_to_cart" value="Add to Cart">
                    <hr>
                </form>
        <?php }
        } ?>
        <a href="autController.php?action=logout">LOGOUT</a>
        <div style="clear:both ;">
            <h3>Order details</h3>
            <table class="table">
                <tr>
                    <th>Item name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
                <?php
                if (!empty($_SESSION["shopping_cart2"])) {
                    $total = 0;
                    foreach ($_SESSION["shopping_cart2"] as $keys => $values) {
                        
                ?>
                        <tr>
                            <td><?= $values["item_name"]; ?></td>
                            <td><?php echo $values["item_quantity"]; ?></td>
                            <td><?php echo $values["item_price"]; ?></td>
                            <td><?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
                            <td><a href="buyer.php?action=delete&id=<?php echo $values["item_id"]; ?>">Remove</a></td>


                        </tr>
                    <?php
                        $total = $total + ($values["item_quantity"] * $values["item_price"]);
                    }
                    ?>
                    <tr>
                        <td style="text-align: right;">Total</td>
                        <td style="text-align: right;"><?php echo number_format($total, 2) ?> din</td>
                    </tr>
                <?php
                }
                ?>
                ?>
            </table>
            <?= $msg  ?>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</html>