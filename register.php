<?php
$msg = isset($msg) ? $msg : '';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/index-style.css">
    <title>Document</title>
</head>

<body>
    <div class="login-box">
        <h2>Register</h2>
        <form action="autController.php" method="POST">
            <div class="user-box">
                <input type="text" name="first_name" value="<?= isset($first_name) ? $first_name : '' ?>">
                <label>First Name</label>
            </div>
            <div class="user-box">
                <input type="text" name="last_name" value="<?= isset($last_name) ? $last_name : '' ?>">
                <label>Last Name</label>
            </div>
            <div class="user-box">
                <input type="text" name="email" value="<?= isset($email) ? $email : '' ?>">
                <label>E-mail</label>
            </div>
            <div class="user-box">
                <input type="text" name="username" value="<?= isset($username) ? $username : '' ?>">
                <label>Username</label>
            </div>
            <div class="user-box">
                <input type="password" name="password" value="<?= isset($password) ? $password : '' ?>">
                <label>Password</label>
            </div>
            <div class="user-box">
                <input type="password" name="repeat_password" value="<?= isset($repeat_password) ? $repeat_password : '' ?>">
                <label>Repeat Password</label>
            </div>
            <label style="color: white;"> Not a robot: </label>
            <input type="checkbox" name="robot" value="yes"><br><br>
            <input type="submit" value="REGISTER" name="action" class="submit"><br><br>
            <a class="register" style="color:white" href="index.php"> Already have account?Login!</a><br>
            <?= $msg ?>

        </form>
    </div>
</body>

</html>